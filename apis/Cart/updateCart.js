// UPDATE CART INFORMATION OF PARTICULAR CARTID
const { sqlconnection } = require("../../config/connection");
module.exports.updateCart = async (event) => {
  let data = JSON.parse(event.body);
  let sql = await sqlconnection();
  // create Request object
  var request = sql.request();

  // query to the database and get the records
  return await request
    .input("jsonparam", event.body)
    .input("mode", "UpdateCart")
    .execute("spcart")
    .then((data) => {
      JSON.stringify(data.recordset);
      return {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept",
        },
        body: JSON.stringify(
          {
            message: "success",
            status:true,
            data: recordset.recordsets[0],
          },
          null,
          2
        ),
      };
    }).catch(()=>{
      return {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept",
        },
        body: JSON.stringify(
          {
            message: "error",
            status:false,
            data: [],
          },
          null,
          2
        ),
      };
    })
};

// this.execute().then((col) => {
//     console.log("coll" + JSON.stringify(col))
// })
