const { sqlconnection } = require("../../../config/connection");

module.exports.getsellerbydate = async (event) => {
  let sql = await sqlconnection();
  let request = sql.request();
  return await request
    .input("jsonparam", event.body)
    .input("mode", "GetSellerByDate")
    .execute("[dbo].[spgetseller]")
    .then((recordset) => {
      return {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "auth-token, Access-Control-Allow-Origin, X-Requested-With, Content-Type, Accept",
          "Access-Control-Allow-Credentials": true,
        },
        body: JSON.stringify(
          {
            message: "success",
            status: true,
            data: recordset.recordsets[0],
          },
          null,
          2
        ),
      };
    })
    .catch((err) => {
      return {
        statusCode: 400,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept",
        },
        body: JSON.stringify(
          {
            message: "error",
            status: false,
            data: err,
          },
          null,
          2
        ),
      };
    });
};
