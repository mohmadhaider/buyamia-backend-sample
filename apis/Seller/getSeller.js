// GET ALL SELLER INFORMATION
const { sqlconnection } = require("../../config/connection");
module.exports.getSeller = async (event) => {

  let sql = await sqlconnection();
  var request = sql.request();
  return await request.input("jsonparam",event.body)
    .input("mode", "GetAllSeller")
    .execute("spgetseller").then((recordset) => {
      return {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept",
        },
        body: JSON.stringify(
          {
            message: "success",
            status:true,
            data: recordset.recordsets[0],
          },
          null,
          2
        ),
      };
    }).catch(()=>{
      return {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept",
        },
        body: JSON.stringify(
          {
            message: "error",
            status:false,
            data: [],
          },
          null,
          2
        ),
      };
    })
};
