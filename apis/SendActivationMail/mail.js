"use strict";

const { transporter } = require("../../config/mail");

module.exports.mail = async (data) => {
  console.log(data);
  const mailBody = {
    from: "htt@fahm-technologies.com",
    to: data,
    subject: "Agreement From Himachal Tour & Travels",
    // attachments: [
    //   {
    //     filename: "YatraMarketPlaceActivitiesSupplierAgreement.pdf",
    //     path: "https://httdemo.s3.ap-south-1.amazonaws.com/YatraMarketPlaceActivitiesSupplierAgreement.pdf",
    //   },
    // ],
    html: `<div className="email" style="
        border: 1px solid black;
        padding: 20px;
        font-family: sans-serif;
        line-height: 2;
        font-size: 20px; 
        ">
        <h2>Agreement From Himachal Tour & Travels!</h2>
         </div>
         <p>https://httdemo.s3.ap-south-1.amazonaws.com/YatraMarketPlaceActivitiesSupplierAgreement.pdf</p>
    `,
  };
  return new Promise(async (res, reject) => {
    await transporter.sendMail(mailBody, (err, result) => {
      if (err) {
        const data = {
          statusCode: 400,
          status: "ERROR",
          result: err,
        };
        res(data);
      } else {
        const tempData = {
          statusCode: 200,
          status: "SUCCESS",
          result: result,
        };
        res(tempData);
      }
    });
  });
};
