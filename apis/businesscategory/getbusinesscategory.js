//GET ALL BUSINESS CATEGORY
const { sqlconnection } = require("../../config/connection");

module.exports.getbusinesscategory = async (event, context) => {
  let sql = await sqlconnection();
  // create Request object
  var request = sql.request();
  return await request
    .input("mode", "GetAllBusinessCategory")
    .execute("[dbo].[spgetbusinesscategory]")
    .then((recordset) => {
      return {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept",
        },
        body: JSON.stringify(
          {
            message: "success",
            status: true,
            data: recordset.recordsets[0],
          },
          null,
          2
        ),
      };
    })
    .catch(() => {
      return {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept",
        },
        body: JSON.stringify(
          {
            message: "error",
            status: false,
            data: [],
          },
          null,
          2
        ),
      };
    });
};
