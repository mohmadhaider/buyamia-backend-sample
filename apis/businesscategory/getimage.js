//GET ALL BUSINESS CATEGORY
const { sqlconnection } = require("../../config/connection");

module.exports.getimage = async () => {
  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers":
        "Origin, X-Requested-With, Content-Type, Accept",
    },
    body: JSON.stringify(
      {
        message: "success",
        status: true,
        data: [
          {
            image:
              "https://assets.ajio.com/medias/sys_master/images/images/hb1/had/31254779985950/Ajio-SS21-BANNERS-1024x672px-01.jpg",
          },
          {
            image:
              "https://i.pinimg.com/originals/df/c3/c7/dfc3c74d5f1f964dcd630f5129951a5b.jpg",
          },
          {
            image:
              "https://image.freepik.com/free-vector/cosmetics-beauty-products-makeup-sale-banner-with-glowing-neon-background-pink-sparkles_33099-1429.jpg",
          },
          {
            image:
              "https://image.freepik.com/free-vector/realistic-christmas-sale-banner-with-red-page_1361-3133.jpg",
          },
        ],
      },
      null,
      2
    ),
  };
};
