// INSERT INVOICE INFORMATION

const { sqlconnection } = require("../../config/connection");


module.exports.insertinvoice = async (event, context) => {
    let sql = await sqlconnection()
    // create Request object
    var request = sql.request();

    //calling a stored procedure  
    return await request
    .input("jsonparam", event.body)
    .input("mode", "InsertInvoice")
        .execute('[dbo].[spinvoice]')
        .then((recordset) => {
            return {
                statusCode: 200,
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Headers":
                        "Origin, X-Requested-With, Content-Type, Accept",
                },
                body: JSON.stringify(
                    {
                        message: "success",
                        status:true,
                        data: recordset.recordsets[0],
                    },
                    null,
                    2
                ),
            };
        }).catch(()=>{
            return {
              statusCode: 200,
              headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers":
                  "Origin, X-Requested-With, Content-Type, Accept",
              },
              body: JSON.stringify(
                {
                  message: "error",
                  status:false,
                  data: [],
                },
                null,
                2
              ),
            };
          })
}