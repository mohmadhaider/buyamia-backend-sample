// GET BUYER INFORMATION  CITY WISE
const { sqlconnection } = require("../../config/connection");

module.exports.authenticationCheck = async (event) => {
  let sql = await sqlconnection();
  let request = sql.request();
  const jwt = require("njwt");
  const claims = { iss: "fun-with-jwts", sub: "AzureDiamond" };
  const token = jwt.create(claims, "top-secret-phrase");
  const tempToken = token.compact();

  return await request
    .input("activationtoken", event.pathParameters.data)
    .input("authtoken", tempToken)
    .execute("[dbo].[spauthcheck]")
    .then((recordset) => {
      // recordset.status(301).redirect("https://www.google.com");

      return {
        statusCode: 302,
        headers: {
          Location: "https://www.google.com/",
        },
      };
    })
    .catch((err) => {
      return {
        statusCode: 400,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept",
        },
        body: JSON.stringify(
          {
            message: "error",
            status: false,
            data: err,
          },
          null,
          2
        ),
      };
    });
};
