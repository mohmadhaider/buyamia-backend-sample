// INSERT PORTAL USER INFORMATION
const { sqlconnection } = require("../../config/connection");
let status = false;
function mailsentcheck(val) {
  // console.log(val);
  status = val;
}
module.exports.insertportaluser = async (event, context) => {
  let sql = await sqlconnection();
  // create Request object
  const jwt = require("njwt");
  const claims = { iss: "fun-with-jwts", sub: "AzureDiamond" };
  const token = jwt.create(claims, "top-secret-phrase");
  const tempToken = token.compact();
  const axios = require("axios");
  var request = sql.request();
  const data = JSON.parse(event.body);
  // console.log(data.emailid);
  return await request
    .input("jsonparam", event.body)
    .input("mode", "InsertPortalUser")
    .input("activationtoken", tempToken)
    .execute("[dbo].[spportaluser]")
    .then(async (recordset) => {
      return await axios
        .post(
          "http://ec2-13-126-109-93.ap-south-1.compute.amazonaws.com:4000/send_activation_mail",
          {
            mail: data.emailid,
            token: tempToken,
          }
        )
        .then((res) => {
          // console.log(res);
          // console.log(res.data.body.accepted.length);
          if (res.data.body.accepted.length >= 1) {
            return {
              statusCode: 200,
              headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers":
                  "Origin, X-Requested-With, Content-Type, Accept",
              },
              body: JSON.stringify(
                {
                  message: "success",
                  status: true,
                  data: {
                    data: "Mail Sent Successfully",
                    // token: tempToken,
                  },
                },
                null,
                2
              ),
            };
          } else {
            return {
              statusCode: 400,
              headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers":
                  "Origin, X-Requested-With, Content-Type, Accept",
              },
              body: JSON.stringify(
                {
                  message: "error",
                  status: false,
                  data: "Mail Not sent",
                },
                null,
                2
              ),
            };
          }
        })
        .catch((error) => {
          return {
            statusCode: 400,
            headers: {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers":
                "Origin, X-Requested-With, Content-Type, Accept",
            },
            body: JSON.stringify(
              {
                message: "error",
                status: false,
                data: error,
              },
              null,
              2
            ),
          };
        });
    })
    .catch((err) => {
      return {
        statusCode: 400,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept",
        },
        body: JSON.stringify(
          {
            message: "error",
            status: false,
            data: err,
          },
          null,
          2
        ),
      };
    });
};
